from django.test import Client
from django.test import TestCase
from django.contrib.auth.models import User

class LoginViewTest(TestCase):

    def test_success_login(self):
        # Instantiate client
        self.client = Client()

        # Create test  user
        self.username = 'test'
        self.email = 'test@test.com'
        self.password = 'test123456'
        User.objects.create_user(self.username, self.email, self.password)

        # Assertions
        login = self.client.login(username = self.username, password=self.password)
        self.assertEqual(login, True, 'Login es ' + str(login) + ' y se esperaba True')

    def test_unsuccess_login(self):
        # Instantiate client
        self.client = Client()

        # Assertions
        login = self.client.login(username = 'baduser', password='badpass')
        self.assertEqual(login, False, 'Login es ' + str(login) + ' y se esperaba False')

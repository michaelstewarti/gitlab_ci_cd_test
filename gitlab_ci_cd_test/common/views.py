from django.shortcuts import render
from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin
from common.forms import LoginForm


class LoginView(TemplateView):
    template_name = "login.html"

    def get(self, request, *args, **kwargs):
        next = request.GET.get('next', '/')
        kwargs['next'] = next

        return super(LoginView, self).get(request, *args, **kwargs)

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect('/')
        return super(LoginView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = LoginForm(request.POST, request=request)
        if form.is_valid():
            user = authenticate(username=request.POST.get('username'), password=request.POST.get('password'))
            if user is not None:
                # solo los que son miembros del staff pueden logearse al webapp
                if user.is_staff and user.is_active:
                    login(request, user)
                    return HttpResponseRedirect(request.POST.get('next','/'))
                else:
                    return render(request, "login.html", {
                        "error": True,
                        "message": "Su cuenta se encuentra inactiva. Favor contactar al administrador"
                    })
            else:
                return render(request, "login.html", {
                    "error": True,
                    "message": "No se encontro su cuenta. Favor contactar al administrador"
                })
        else:
            return render(request, "login.html", {
                "error": True,
                "message": "Usuario o contrasena incorrectos. Favor intentar de nuevo."
            })

class HomeView(LoginRequiredMixin, TemplateView):
    login_url = '/login/'
    template_name = "index.html"
